require 'nn'
require 'image'
require 'torch'
require 'xlua'


local train_dir = '.'
os.execute('cd ' .. train_dir)

--------------------------------------------------------------------------------
--                            LOAD DATA
--------------------------------------------------------------------------------

local imagesAll = torch.Tensor(1000,3,96,96)
local labelsAll = torch.Tensor(1000)
in_size = 96

classes = {'face', 'backg'}

-- pozadine
for i=1,500 do
  imagesAll[i] = image.load('/home/vedran/WIDER_train/bg/bg'..i..'.png', 'float'):float()
  labelsAll[i]= 2 -- 1 = background
end

--lica
for i=500,1000 do
  imagesAll[i] = image.load('/home/vedran/WIDER_train/faces/face'..i..'.png'):float()
  labelsAll[i] = 1
end

-- shuffle dataset: get shuffled indices in this variable:
local labelsShuffle = torch.randperm((#labelsAll)[1])

local portionTrain = 0.8 -- 80% is train data, rest is test data
local trsize = torch.floor(labelsShuffle:size(1)*portionTrain)
local tesize = labelsShuffle:size(1) - trsize

-- 1-dimenzionalni input slike!!
trainData = {
   data = torch.Tensor(trsize, 3, in_size, in_size),
   labels = torch.Tensor(trsize),
   size = function() return trsize end
}
--create test set:
testData = {
      data = torch.Tensor(tesize, 3, in_size, in_size),
      labels = torch.Tensor(tesize),
      size = function() return tesize end
}

   -- Zasto se ne uzimaju sve 3 komponente slike ??
   for i=1,trsize do
      --trainData.data[i] = imagesAll[labelsShuffle[i]][1]:clone()
      trainData.data[i] = imagesAll[labelsShuffle[i]]:clone()
      trainData.labels[i] = labelsAll[labelsShuffle[i]]
   end

   for i=trsize+1,tesize+trsize do
      --testData.data[i-trsize] = imagesAll[labelsShuffle[i]][1]:clone()
      testData.data[i-trsize] = imagesAll[labelsShuffle[i]]:clone()
      testData.labels[i-trsize] = labelsAll[labelsShuffle[i]]
   end

   trainData.data = trainData.data:float()
   testData.data = testData.data:float()

   --Convert images to YUV
   print '==> preprocessing data: colorspace RGB -> YUV'
   for i = 1,trainData:size() do
    trainData.data[i] = image.rgb2yuv(trainData.data[i])
   end

   for i = 1,testData:size() do
     testData.data[i] = image.rgb2yuv(testData.data[i])
   end

   -- Name channels for convenience
local channels = {'y'}--,'u','v'}

print(sys.COLORS.red ..  '==> preprocessing data: normalize each feature (channel) globally')
local mean = {}
local std = {}
for i,channel in ipairs(channels) do
   -- normalize each channel globally:
   mean[i] = trainData.data[{ {},i,{},{} }]:mean()
   std[i] = trainData.data[{ {},i,{},{} }]:std()
   trainData.data[{ {},i,{},{} }]:add(-mean[i])
   trainData.data[{ {},i,{},{} }]:div(std[i])
end

-- Normalize test data, using the training means/stds
for i,channel in ipairs(channels) do
   -- normalize each channel globally:
   testData.data[{ {},i,{},{} }]:add(-mean[i])
   testData.data[{ {},i,{},{} }]:div(std[i])
end

-- Local contrast normalization is needed in the face dataset as the dataset is already in this form:
print(sys.COLORS.red ..  '==> preprocessing data: normalize all three channels locally')

-- Define the normalization neighborhood:
local neighborhood = image.gaussian1D(5) -- 5 for face detector training

-- Define our local normalization operator (It is an actual nn module,
-- which could be inserted into a trainable model):
local normalization = nn.SpatialContrastiveNormalization(1, neighborhood, 1):float()

-- Normalize all channels locally:
for c in ipairs(channels) do
   for i = 1,trainData:size() do
      trainData.data[{ i,{c},{},{} }] = normalization:forward(trainData.data[{ i,{c},{},{} }])
   end
   for i = 1,testData:size() do
      testData.data[{ i,{c},{},{} }] = normalization:forward(testData.data[{ i,{c},{},{} }])
   end
end

--------------------------------------------------------------------------------
print(sys.COLORS.red ..  '==> visualizing data')

visualize = false
if visualize then
   local first256Samples_y = trainData.data[{ {1,64},1 }]
   image.display{image=first256Samples_y, nrow=8, legend='Some training examples: Y channel'}
   --local first256Samples_y = testData.data[{ {1,128},1 }]
   --image.display{image=first256Samples_y, nrow=16, legend='Some testing examples: Y channel'}
end


return {
      traindata = trainData,
      testdata  = testData,
      classes = classes
}
