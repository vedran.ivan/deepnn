require 'nn'
require 'image'
require 'torch'

local cnn = nn.Sequential()
local n = 2
local K = 1
local spool = 1 --pool size

---------------------------------------------------------------------------------
--                            MODEL
--------------------------------------------------------------------------------

--256 x 256 x 3 input
--layer parameters
local in_planes = 3
local out_planes = 3
local kw = 3 --kernel width
local kh = 3 --kernel height
local dw = 1 --conv stride x
local dh = 1 --conv stride y

--96x96 ulaz
--layer1 input = 3x96x96
cnn:add(nn.SpatialConvolution(in_planes,out_planes, 3, 3))
cnn:add(nn.ReLU())
--output 3x94x94
cnn:add(nn.SpatialMaxPooling(2,2,2,2))
--output = (input - pool_width)/stride = 3x47x47

--layer2
--layer2 input = 3x47x47
cnn:add(nn.SpatialConvolution(in_planes,out_planes, 3, 3))
--output = 3x45x45
cnn:add(nn.ReLU())
cnn:add(nn.SpatialMaxPooling(2,2,2,2))
--output2 =   3x22x22


--reshape into 1D tensor of size 1452
--cnn:add(nn.View(3*22*22))

cnn:add(nn.Reshape(3*22*22))

cnn:add(nn.Linear(1452, 2))

cnn:add(nn.LogSoftMax())

--------------------------------------------------------------------------------
--                           LOSS CRITERION
--------------------------------------------------------------------------------
loss_criterion = nn.ClassNLLCriterion()
--------------------------------------------------------------------------------
--                        DESCENT ALGORITHM
--------------------------------------------------------------------------------
trainer = nn.StochasticGradient(cnn, criterion)
trainer.learningRate = 0.01
--trainer.maxIteration = 200  ....200 epochs

--------------------------------------------------------------------------------
--                        TRAINING
--------------------------------------------------------------------------------
--my_out = cnn:forward(torch.Tensor(3,96,96))
--print( cnn.modules )

torch.save('cnn_face_model', cnn)

cnn:float()

return{
  model = cnn,
  loss = loss_criterion,
}
