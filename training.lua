require 'torch'
require 'image'
require 'nn'
require 'optim'
require 'gnuplot'
require 'os'
require 'xlua'


--------------------------------------------------------------------------------
local m = require 'model'
local model = m.model
local loss = m.loss
local parameters, gradParameters = model:getParameters()
--------------------------------------------------------------------------------
-- Save light network tools:
function nilling(module)
   module.gradBias   = nil
   if module.finput then module.finput = torch.Tensor() end
   module.gradWeight = nil
   module.output     = torch.Tensor()
   if module.fgradInput then module.fgradInput = torch.Tensor() end
   module.gradInput  = nil
end

function netLighter(network)
   nilling(network)
   if network.modules then
      for _,a in ipairs(network.modules) do
         netLighter(a)
      end
   end
end
--------------------------------------------------------------------------------
local confusion = optim.ConfusionMatrix(classes)

local trainLogger = optim.Logger(paths.concat('results', 'train.log'))

--------------------------------------------------------------------------------
--TODO prepare minibatch
print(sys.COLORS.red ..  '==> allocating minibatch memory')
local batch_in = torch.Tensor(opt.batchSize,trainData.data:size(2),
         trainData.data:size(3), trainData.data:size(4)) --faces data

local batch_target = torch.Tensor(opt.batchSize)
--------------------------------------------------------------------------------

local optimState = {
   learningRate = opt.learningRate,
   momentum = opt.momentum,
   weightDecay = opt.weightDecay,
   learningRateDecay = opt.learningRateDecay
}

local epoch = 1
local index = 0

local function train(trainData)
    epoch = epoch or 1
    local shuffle = torch.randperm(trainData:size())
    -------------------------------------------------
    local time = sys.clock()
    -------------------------------------------------

    print('==> doing epoch on training data:')
	  print("==> online epoch # " .. epoch .. ' [batchSize = ' ..opt.batchSize .. ']')

    for t=1,trainData:size(),opt.batchSize do
        xlua.progress(t, trainData:size())
        collectgarbage()

        if (t + opt.batchSize - 1) > trainData:size() then
         break
        end
        --local start_index = t
        --local end_index = math.min(trainData:size(), t * opt.batchSize)

        local idx = 1
        for i=t,t+opt.batchSize-1 do
          batch_in[idx]     = trainData.data[shuffle[i]]
          batch_target[idx] = trainData.labels[shuffle[i]]
          idx = idx + 1
        end

        local eval_E = function(x)
            if x ~= parameters then
          		 parameters:copy(x)
            end
            --set gradient parameters to 0
            gradParameters:zero()

            local y = model:forward(batch_in)
            --print(#y, #batch_target)
            local E = loss:forward(y, batch_target)

            --calculate gradParameters
            local dE_dw = loss:backward(y,batch_target)
            --update parameters
            model:backward(batch_in, dE_dw)

            -- update confusion
            for i = 1,opt.batchSize do
              confusion:add(y[i],batch_target[i])
            end

            return E, gradParameters
        end
        optim.sgd(eval_E, parameters, optimState)
    end
    -------------------------------------------------------
    epoch = epoch + 1

    time = sys.clock() - time
    time = time/ trainData:size()
    print("\n==> time to learn 1 sample = " .. (time*1000) .. 'ms')

    print(confusion)
    trainLogger:add{['% mean class accuracy (train set)'] = confusion.totalValid * 100}
   if false then
      trainLogger:style{['% mean class accuracy (train set)'] = '-'}
      trainLogger:plot()
    end

    -- save/log current net
    local filename = paths.concat(opt.save, 'model.net')
    os.execute('mkdir -p ' .. sys.dirname(filename))
    print('==> saving model to '..filename)
    model1 = model:clone()
    netLighter(model1)
    torch.save(filename, model1)


    confusion:zero()

end


return train
